package Observer;

import Observable.WeatherStation;

public class PhoneDisplay extends Observer {
	public PhoneDisplay(WeatherStation station) {
		super(station);
	}

	@Override
	public void display(int tem) {
		System.out.println("Phone Display: " + tem);
	}

}
