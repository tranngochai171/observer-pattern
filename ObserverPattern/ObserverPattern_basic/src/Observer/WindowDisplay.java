package Observer;

import Observable.WeatherStation;

public class WindowDisplay extends Observer {

	public WindowDisplay(WeatherStation station) {
		super(station);
	}

	@Override
	public void display(int tem) {
		System.out.println("Window display: " + tem);
	}

}
