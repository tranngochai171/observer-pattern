package Observer;

import Observable.WeatherStation;

public abstract class Observer implements IDisplay {
	protected WeatherStation station;

	public Observer(WeatherStation station) {
		this.station = station;
	}

	public void update() {
		this.display(station.getTemperature());
	}
}
