package Observable;

import java.util.LinkedList;
import java.util.List;

import Observer.Observer;

public class WeatherStation implements IObservable {
	private List<Observer> list;
	private int temperature;

	public int getTemperature() {
		return temperature;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	public WeatherStation() {
		this.list = new LinkedList<Observer>();
	}

	@Override
	public void Add(Observer o) {
		this.list.add(o);
	}

	@Override
	public void Remove(Observer o) {
		this.list.remove(o);
	}

	@Override
	public void Notify() {
		for (Observer item : this.list)
			item.update();
	}

	public void stationChangeValue(int tem) {
		this.temperature = tem;
		this.Notify();
	}
}
