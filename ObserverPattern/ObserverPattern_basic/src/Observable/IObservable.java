package Observable;

import Observer.Observer;

public interface IObservable {
	public void Add(Observer o);

	public void Remove(Observer o);

	public void Notify();
}
