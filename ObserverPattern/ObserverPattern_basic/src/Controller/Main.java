package Controller;

import Observable.WeatherStation;
import Observer.PhoneDisplay;
import Observer.WindowDisplay;

public class Main {
	public static void main(String[] args) {
		WeatherStation ws = new WeatherStation();
		ws.Add(new PhoneDisplay(ws));
		ws.Add(new WindowDisplay(ws));
		ws.Add(new PhoneDisplay(ws));
		ws.stationChangeValue(15);
		ws.stationChangeValue(20);
	}
}
