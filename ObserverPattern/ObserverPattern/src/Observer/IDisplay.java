package Observer;

public interface IDisplay {
	public void display(int temperature);
}
