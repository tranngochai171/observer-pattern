package Observer;

import java.lang.reflect.ParameterizedType;

import Obserable.WeatherStation;

public class GenericDisplay<T> implements IObserver, IDisplay {
	private WeatherStation station;
	private Class<? extends T> clazz;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public GenericDisplay(WeatherStation station) {
		ParameterizedType pt = (ParameterizedType) getClass().getGenericSuperclass();
		this.clazz = (Class) pt.getActualTypeArguments()[0];
		this.station = station;
	}

	@Override
	public void display(int temperature) {
		System.out.println(this.clazz.getSimpleName() + " display: " + temperature);
	}

	@Override
	public void update() {
		this.display(station.getTemperature());
	}

}
