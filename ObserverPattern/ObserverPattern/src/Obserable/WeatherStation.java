package Obserable;

import java.util.LinkedList;
import java.util.List;

import Observer.IObserver;

public class WeatherStation implements IObservable {
	private List<IObserver> observers;

	private int temperature;

	public int getTemperature() {
		return temperature;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
		this.Notify();
	}

	public List<IObserver> getObservers() {
		return observers;
	}

	public WeatherStation() {
		this.observers = new LinkedList<IObserver>();
	}

	public void setObservers(List<IObserver> observers) {
		this.observers = observers;
	}

	@Override
	public void Add(IObserver o) {
		this.observers.add(o);

	}

	@Override
	public void Remove(IObserver o) {
		this.observers.remove(o);

	}

	@Override
	public void Notify() {
		for (IObserver item : this.observers) {
			item.update();
		}
	}

}
