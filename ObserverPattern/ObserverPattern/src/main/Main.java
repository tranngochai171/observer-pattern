package main;

import Obserable.WeatherStation;
import Observer.PhoneDisplay;
import Observer.WindowDisplay;

public class Main {

	public static void main(String[] args) {
		WeatherStation station = new WeatherStation();
		station.Add(new PhoneDisplay(station));
		station.Add(new WindowDisplay(station));
		station.setTemperature(16);
		station.setTemperature(20);
	}

}
